# Python Resources

## Books

### Python Basics
- [Learn Python in One Day](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/GspTzJrD)
- [Python for Dummies](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/CpgljBxJ)
- [Illustrated Guide for Python](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/ipwlHCSR)
- [Python Basics: Self Teaching Introduction](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/C5xzVKzb)
### Hacking
- [Ethical Hacking With Python](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/KlIGQQCL)
- [For Beginners: Python and Hacking](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/OtA2VKJR)
### Machine Learning
- [Machine Learning Cookbook](https://mega.nz/folder/6txy2AgB#Q8sxFZuctbm1BFRWyVX-BQ/file/HtQA0aJB)

## Videos

### Beginner
- [CS Dojo: What can you do with Python?](https://www.youtube.com/watch?v=kLZuut1fYzQ)
- [FreeCodeCamp: Python for Beginners](https://www.youtube.com/watch?v=rfscVS0vtbw)
