# Overview

  

Welcome to Python Basics! We'll be covering the most essential aspects of python in this tutorial.

  

These include:

  

- variables

- conditional logic and operators

- loops

- functions

- classes

- documenting

- imports

- python package system (PIP)

  
  

## Variables

  
Variables are the primary way to store information in a programming language. This is how you can let a function know what number to double, or another file know which value to import from a file.

There are 2 main types of variables. These include regular variables and constants.

  
### Regular Variables
Regular variables are declared by putting an equal sign between an identifier `a` with a value `5`.

`a = 5` 
`b="hello world"`

These would all be valid declarations of a variable. Here `a` would have a value of 5. `b` would have a value of "hello world."

**Remember:** Python does not have a delimiter to separate lines like a `;`. You don't have to put a semicolon to tell python that you have finished assigning a variable.

### Constants

Unlike other languages, python does not have native support for constants. You could create a class that acts as a constant or make a separate file/module to store all constants however.

```py
# Creating constants via a file
--- Constants.py ---
# Create a file called Constants.py with all your constants 
# (Standard is to capitalize constants)
PI = 3.14159265358979323
API_KEY = 'jfk392jr02-f924mk'
SESSION_LIFETIME = '30s'

--- Main.py ---
# import the Constants file you have made
import Constants.py
# Use your constant!
print(Constants.PI)
```

### Types

Before going any further with variables. It is important that you get a basic understanding of the types each variable can be associated with. Each type represents a particular signature of a variable (is it a number, letter, etc), which helps the interpreter know how to operate on these variables.

Base Types: (the brackets give the acronym for the type)
- Integer [int]  (a number)
- String [str] (a single, or group of letters)
- Boolean [bool] (a value indicating True or False)
- NoneType [none] (a type indicating nothingness. It basically doesn't exist. Confusing right?)

```py
#Basic Python Types
age = 5 #int
name = "Jake" | 'Jake' #str. NOTE: you can use either ' or " to declare a value as a string.
tall = True #bool. Can be either True or False
unknown = None #use None to declare a NoneType.
```

Variables of different Types can generally not be operated on together. You want to use the same type when running an operation (or interpreter will send red errors at you)

```py
val = 10
color = 'blue'

# try adding val and color. 
result = val + color #ERROR: interpeter doesn't know how to add int and string!

# instead convert val to string first using str function
result = str(val) + color #result = 10blue
```

There are also a number of class types that are prebuilt or you can make.

Class Types:
- Dict [dict] (A data structure that contains a key and a value. Each key is mapped to a value)
- List [list] (A "list" of values)
- Tuple [tuple] (Also a list of values. But cannot be edited. Essentially a constant list)
- Range [range] (A range of values. Has a particular order unlike list)
- Set [set] (A list of values, but they must be unique)
- ...
- Custom Class (ex. Animal Class)

```py
new_dict = dict({'name':'Jake'}) #a dictionary with key name mapped to Jake.
# Note all keys are strings. So must be declared as str with ' or "

new_list = list([1,2,3]) | [1,2,3] #Creates a list of values containing 1, 2, 3
#Note list can be created by just placing a series of values 
#List can be created either using list() or brackets []

new_tuple = (1,2,3) #A list of values but not editable

new_range = range(1,5) #Creates a range of values from 1~5

new_set = set([1, 2, 2, 5]) #Creates a set of (1,2,5). 
# All elements are unique, so the duplicate 2 is removed.
```

You can also create your own type by making your own class (dict, set, list are all  builtin classes in python)

```py
#Create a New Animal Class -> Creates a new Animal Type
class Animal {
	def __init__(self, name) {
		#create Animal object with name you supplied
		self.name = name
	}
}

#If we now create an Animal Object. This object will have type Animal
new_animal = Animal('cheetah')
print(type(new_animal)) # output: Animal

```

--- 
### Using Variables

Once you have assigned variables, you can start using them! You can print them out to the console, run mathematical operations on them... The options are endless.

Think of variables as simply a box that holds a piece of information. You pass this box around to different areas of your code so that they operate on the correct piece of information.

You can do many things with variables. 
- You could add two variables: 
	```py
	v1 = 5
	v2 = 10
	print(v1 + v2) # Output: 15
	
	s1 = 'Sally'
	s2 = 'James'
	print(s1 + s2) #Output: SallyJames
	```
- pass variable to a function
	```py
	def double(x) {
		return 2 * x
	}
	v = 5
	print(double(v)) #Output: 10
	```
- even assign another variable to have the value of your current variable
	```py
	var1 = set([1,2,3,4,5])
	var2 = var1 
	#var2 is now a set containing the unique values of 1,2,3,4,5
	```

*Remember: You should check the python documentation to see all the operations with variables python supports.*

Here are the main ones however:
- Mathematical operations for integers (+, -, *, /, % - modulo, etc)
- Adding strings (concatenates strings)
- Comparing variables (< less than, > greater than, = equals, != not equals, bitwise operators)
- Assigning value of variable to another
- Checking if a variable is of a certain type (`isinstance(v, int)` checks whether v is a type int)
- More [here](https://docs.python.org/3/library/stdtypes.html)

