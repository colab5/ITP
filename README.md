# Intro to Python

Introduction and basics to python. It's a simple and beginnner-friendly language, so be sure to check it out!

## Part one: The Beginning 

Python was a general purpose language created by Guido Van Rossum in the late 1980's and released to the general public in 1991. It was intended to be the successor of the ABC language, which was also a general purpose language, and many of its properties and ideas are borrowed from the ABC Language.

Starting from the 1990's Python's popularity has grown immensely over the years. Now it is the second most popular language, having beat Java recently. This growth has largely been due to Python's low barrier of entry for beginners. Because it is a dynamically typed language and supports many paradigms of programming, including Object oriented, functional, and structured, there are very few rules for beginners to follow.

However, this flexibility, particularly the dynamically typed nature of python, lends itself to be more error prone. Because checks are not run during compile time, many errors in python code will be missed until the program is actually run. This can lead to program crashes, unexpected behaviour, or simply slow execution. To find these runtime bugs you may have to do extensive bug testing. However, in most cases the bugs will be quite simple, like accidently adding the string `s` to the number `5`. Pssst... you can't add a letter to number.

But while Python does not have the advantages of a type checking, it is still much easier to program in than practically any other language on the planet. It comes with a vast array of libraries that can solve most algorithms and problems for you, and handles the issues of garbage collection on its own. In other statically typed languages, like C++, you have to do this. Python is also easy to read due to its usage of whiespace, which makes it easier to program in.

Python's whitespace is a key defining feature of the language. Unlike other languages, which separate functions and lines in the program using brackets and semicolors, python uses whitespace. 4 spaces (or an indent) is equal to the next line in an indented block like a function, and each line of code is automatically interpreted to be a separate line of code (you don't need to use semicolons to separate). 

Here's an example of python's whitespace.

```py
def double(x) {
    # 4 spaces represents an indented block
    returnVal = x * 2
    # pressing enter separates the lines of code
    return returnVal
}
```

There have been 4 major versions of python released. After the initial version of python was released in 1991, Python 2 was released 2000 with list comprehension and a cycling garbage collector. In 2008 Python 3 was released without backwards compatability (meaning it has breaking changes from python 2). And most recently, Python 4 was released, which adds a number of useful features to the language.

## Modern Day Python

Python is now used regularly in many applications. 

- Tkinter and PyQt is often used for desktop applications. 
- Django and Flask are two popular frameworks for web applications. 
- Kivy is the mainstream framework for mobile applications
- Python is also heavily used for machine learning (tensorflow, pytorch, etc)


Think you're ready to move onto learning python? Click [here](https://gitlab.com/colab5/ITP/-/blob/master/Part%202:%20Python%20Basics.py)!
